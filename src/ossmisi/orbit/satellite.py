import poliastro
from astropy import units as u


class Satellite:
    def __init__(self, name, orbit: poliastro.twobody.Orbit, cd=2.2, mass=150):
        self.name = name
        self._orbit = orbit
        self.cd = cd  # drag coeff
        self.mass = mass  # * u.kg

    @property
    def orbit(self):
        return self._orbit
