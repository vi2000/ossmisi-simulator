# OSSMISI

[![PyPI - Version](https://img.shields.io/pypi/v/ossmisi.svg)](https://pypi.org/project/ossmisi)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/ossmisi.svg)](https://pypi.org/project/ossmisi)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install ossmisi
```

## License

`ossmisi` is distributed under the terms of the [AGPLv3](https://spdx.org/licenses/AGPL-3.0-or-later.html) license.
